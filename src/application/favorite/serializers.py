# -*- coding: utf-8 -*-
from rest_framework import serializers
from .models import Favorite


class FavoriteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Favorite
        extra_kwargs = {
            'image': {'read_only': True},
            'user': {'read_only': True},
        }
