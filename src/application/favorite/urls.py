from rest_framework import routers

from .views import FavoriteViewSet


router = routers.SimpleRouter()
router.register(r'favorite', FavoriteViewSet)

urlpatterns = router.urls
