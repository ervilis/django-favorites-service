import json
import requests

from django.conf import settings
from django.core.validators import URLValidator


class Client(object):
    """Client for Screenshot API"""

    base_url = settings.SCREENSHOT_API_URL

    def validate_url(self, url):
        # Raises a ValidationError exception if url is invalid
        URLValidator()(url)

    def create_screenshot(self, url):
        self.validate_url(url)

        response = requests.post(self.base_url,
                                 data=json.dumps({'url': url}),
                                 headers={'Content-type': 'application/json'})
        return json.loads(response.content)
