"""
We need disconnect the create_screenshot signal for run another tests
"""
from django.db.models import signals

from application.favorite.models import Favorite
from application.favorite.signals import create_screenshot


signals.pre_save.disconnect(create_screenshot, sender=Favorite)
