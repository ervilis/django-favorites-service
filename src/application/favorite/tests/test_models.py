from tempfile import NamedTemporaryFile
from datetime import datetime

from django.test import TestCase
from django.core.files import File
from django.contrib.auth.models import User

from application.favorite.models import Favorite


class ScreenshotTestCase(TestCase):

    fixtures = ['user.json']

    def test_instance(self):
        self.assertIsInstance(Favorite(), Favorite)

    def test_instance_save(self):
        instance = Favorite(user=User.objects.all()[0],
                            name='Foo bar',
                            url='https://foo.bar/',
                            image=File(NamedTemporaryFile(mode='rb')))
        instance.save()
        self.assertTrue(instance.pk)
        self.assertIsInstance(instance.image.url, (str, unicode))
        self.assertIsInstance(instance.date_insert, datetime)
