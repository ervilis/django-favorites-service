import json
import httpretty

from django.conf import settings
from django.test import TestCase
from django.core.validators import URLValidator, ValidationError

from application.favorite.screenshoter import Client


class ClientTestCase(TestCase):

    def test_instance(self):
        self.assertIsInstance(Client(), Client)

    @httpretty.activate
    def test_create(self):
        expected_response = {
            'id': 1,
            'url': 'http://foo.bar/',
            'image': 'http://screenshot.com.br/media/8c38676af3c7.png',
            'date_insert': '2015-04-27T00:06:32.901429Z'
        }
        httpretty.register_uri(
            httpretty.POST,
            settings.SCREENSHOT_API_URL,
            body=json.dumps(expected_response),
            content_type="application/json"
        )

        client = Client()
        result = client.create_screenshot('http://foo.bar/')

        self.assertTrue('id' in result.keys())
        self.assertTrue('url' in result.keys())
        self.assertTrue('image' in result.keys())
        self.assertTrue('date_insert' in result.keys())

    def test_invalid_url(self):
        client = Client()

        with self.assertRaises(ValidationError):
            client.create_screenshot('http://invalid')
