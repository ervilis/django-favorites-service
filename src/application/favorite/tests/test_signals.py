import json
import mock
import httpretty
from tempfile import NamedTemporaryFile

from django.conf import settings
from django.test import TestCase
from django.core.files import File
from django.contrib.auth.models import User

from application.favorite.models import Favorite
from application.favorite.signals import create_screenshot


class CreateScreenshotTestCase(TestCase):

    fixtures = ['user.json']

    @httpretty.activate
    def test_save(self):
        expected_response = {
            'id': 1,
            'url': 'http://foo.bar/',
            'image': 'http://screenshot.com.br/media/8c38676af3c7.png',
            'date_insert': '2015-04-27T00:06:32.901429Z'
        }
        httpretty.register_uri(
            httpretty.POST,
            settings.SCREENSHOT_API_URL,
            body=json.dumps(expected_response),
            content_type="application/json"
        )

        user = User.objects.all()[0]
        instance = Favorite(user=user, url='http://google.com/', name='Test')

        with mock.patch("requests.get") as get:
            get.return_value.content = ""

            create_screenshot(Favorite, instance)

        instance.save()

        self.assertTrue(instance.pk)
        self.assertIsInstance(instance.image.url, (str, unicode))
