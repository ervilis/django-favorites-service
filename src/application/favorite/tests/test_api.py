import json
from mockups import Mockup

from django.test import TestCase, Client
from django.contrib.auth.models import User

from rest_framework.reverse import reverse

from application.favorite.models import Favorite


RESOURCE_URL = reverse('favorite-list')


def get_user(username='test'):
    return User.objects.get(username=username)


class FavoriteListTestCase(TestCase):

    fixtures = ['user.json']

    def setUp(self):
        self.client = Client()
        user = get_user()
        self.instances = [
            Favorite.objects.create(user=user,
                                    name='Foo site',
                                    url='http://foo.bar/'),
            Favorite.objects.create(user=user,
                                    name='Bar site ',
                                    url='http://bar.foo/'),
        ]
        user2 = get_user('test2')
        Favorite.objects.create(user=user2,
                                name='Foo site',
                                url='http://foo.bar/'),

    def test_list(self):
        self.client.login(username="test", password="test")
        response = self.client.get(RESOURCE_URL)
        data = json.loads(response.content)

        self.assertEquals(response.status_code, 200)

        for instance in self.instances:
            self.assertTrue(filter(lambda x: x['id'] == instance.pk, data))

    def test_list_only_user_objects(self):
        self.client.login(username="test", password="test")
        response = self.client.get(RESOURCE_URL)
        data = json.loads(response.content)

        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(data), 2)

        self.client.login(username="test2", password="test")
        response = self.client.get(RESOURCE_URL)
        data = json.loads(response.content)

        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(data), 1)


class FavoriteCreateTestCase(TestCase):

    fixtures = ['user.json']

    def setUp(self):
        self.client = Client()
        self.client.login(username="test", password="test")

    def test_create_successful(self):
        post_data = {
            'url': 'http://foo.bar/img.jpg',
            'name': 'My favorite website',
        }
        response = self.client.post(RESOURCE_URL,
                                    json.dumps(post_data),
                                    content_type='application/json')
        response_data = json.loads(response.content)

        self.assertEquals(response.status_code, 201)
        self.assertEqual(response_data['url'], post_data['url'])

    def test_validation_error(self):
        response = self.client.post(RESOURCE_URL,
                                    json.dumps({'name': 'without url'}),
                                    content_type='application/json')
        response_data = json.loads(response.content)

        self.assertEquals(response.status_code, 400)
        self.assertTrue('url' in response_data.keys())
        self.assertEqual(response_data['url'][0], "This field is required.")


class FavoriteDeleteTestCase(TestCase):

    fixtures = ['user.json']

    def setUp(self):
        self.client = Client()
        self.client.login(username="test", password="test")

    def test_delete_successful(self):
        user = get_user()
        obj = Favorite.objects.create(user=user,
                                      name='Foo site',
                                      url='http://foo.bar/')

        self.client.login(username="test", password="test")
        response = self.client.delete("{}{}/".format(RESOURCE_URL, obj.pk))

        self.assertEquals(response.status_code, 204)

    def test_delete_invalid(self):
        response = self.client.delete("{}{}/".format(RESOURCE_URL, 999999))

        self.assertEquals(response.status_code, 404)


class FavoriteNotAllowedTestCase(TestCase):

    def test_create_successful(self):
        client = Client()
        response = client.get(RESOURCE_URL)

        self.assertEquals(response.status_code, 403)
