import uuid

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from .signals import create_screenshot


def upload_to(instance, filename):
    return "screenshots/{}.png".format(uuid.uuid4())


class Favorite(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    name = models.CharField(max_length=255)
    url = models.URLField()
    image = models.ImageField(
        _(u"Image"),
        null=True,
        blank=True,
        upload_to=upload_to
    )
    date_insert = models.DateTimeField(_(u"Date insert"), auto_now_add=True)

    __unicode__ = lambda x: x.url


models.signals.pre_save.connect(create_screenshot, sender=Favorite)
