from rest_framework import viewsets, permissions

from .serializers import FavoriteSerializer
from .models import Favorite


class FavoriteViewSet(viewsets.ModelViewSet):
    """A viewset that provides all CRUD operations for Favorite"""

    queryset = Favorite.objects.all()
    serializer_class = FavoriteSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        queryset = super(FavoriteViewSet, self).get_queryset()
        return queryset.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
