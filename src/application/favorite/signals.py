import os
import requests
from tempfile import NamedTemporaryFile

from django.core.files import File

from .screenshoter import Client


def create_screenshot(sender, instance, **kwargs):
    if instance.image:
        return

    client = Client()
    tmpfile = NamedTemporaryFile(mode="wb", delete=False)

    result = client.create_screenshot(instance.url)

    response = requests.get(result['image'], stream=True)
    tmpfile.write(response.content)
    tmpfile.close()

    instance.image = File(open(tmpfile.name, 'rb'))

    os.unlink(tmpfile.name)
