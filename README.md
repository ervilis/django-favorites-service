## Django Favorites API service

Project that provides REST API to manage favorites websites.

### Requirements
This application needs that django-screenshot-service be running on `http://screenshot:8000` url.

NOTE: You can customize this url in `settings.SCREENSHOT_API_URL`

NOTE: You can set `screenshot` host in /etc/hosts.


### System Requirements

`sudo apt-get install python-dev python-pip build-essential`


### Running application
`virtualenv .venv`

`source .venv/bin/activate`

`pip install -r requirements.txt`

`python src/manage.py syncdb`

`python src/manage.py collectstatic --noinput`

`cd src/; gunicorn application.wsgi:application --bind 0.0.0.0:8000`


### Running tests

`pip install -r requirements_dev.txt`

`cd src; python manage.py test`
